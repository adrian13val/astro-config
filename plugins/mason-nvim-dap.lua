return {
  "jay-babu/mason-nvim-dap.nvim",
  opts = {
    handlers = {
      php = function(config)
        local dap = require "dap"

        config.configurations[1].port = 9003
        require('mason-nvim-dap').default_setup(config) -- don't forget this!
      end,
    },
  },
};
