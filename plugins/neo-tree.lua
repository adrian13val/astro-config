return {
  'nvim-neo-tree/neo-tree.nvim',
  opts = {
    window = {
      -- position = 'float',
      mappings = {
        ["<s-tab>"] = "prev_source",
        ["<tab>"] = "next_source",
      }
    },
    filesystem = {
      filtered_items = {
        hide_gitignored = false
      }
    },
    event_handlers = {
      {
        event = "file_opened",
        handler = function(_) vim.cmd("Neotree close") end,
      },
    },
  }
}
