local loadFlutterKeys = function()
  local wk = require("which-key")
  wk.register({
    lF = {
      name = "Flutter",
      s = { "<cmd>FlutterRun<cr>", "Run" },                            -- Run the current project. This needs to be run from within a flutter project.
      d = { "<cmd>FlutterDevices<cr>", "Devices" },                    -- Brings up a list of connected devices to select from.
      e = { "<cmd>FlutterEmulators<cr>", "Emulators" },                -- Similar to devices but shows a list of emulators to choose from.
      x = { "<cmd>FlutterReload<cr>", "Reload" },                      -- Reload the running project.
      X = { "<cmd>FlutterRestart<cr>", "Restart" },                    -- Restart the current project.
      q = { "<cmd>FlutterQuit<cr>", "Quit" },                          -- Ends a running session.
      D = { "<cmd>FlutterDetach<cr>", "Detach" },                      -- Ends a running session locally but keeps the process running on the device.
      o = { "<cmd>FlutterOutlineToggle<cr>", "Outline Toggle" },       -- Toggle the outline window showing the widget tree for the given file.
      O = { "<cmd>FlutterOutlineOpen<cr>", "Outline Open" },           -- Opens an outline window showing the widget tree for the given file.
      t = { "<cmd>FlutterDevTools<cr>", "DevTools" },                  -- Starts a Dart Dev Tools server.
      T = { "<cmd>FlutterDevToolsActivate<cr>", "DevTools Activate" }, -- Activates a Dart Dev Tools server.
      p = { "<cmd>FlutterCopyProfilerUrl<cr>", "Copy Profiler URL" },  -- Copies the profiler url to your system clipboard (+ register). Note that commands FlutterRun and FlutterDevTools must be executed first.
      l = { "<cmd>FlutterLspRestart<cr>", "Lsp Restart" },             -- This command restarts the dart language server, and is intended for situations where it begins to work incorrectly.
      ["^"] = { "<cmd>FlutterSuper<cr>", "Super" },                    -- Go to super class, method using custom LSP method dart/textDocument/super.
      ra = { "<cmd>FlutterReanalyze<cr>", "Reanalyze" },               -- Forces LSP server reanalyze using custom LSP method dart/reanalyze.
      r = { "<cmd>FlutterRename<cr>", "Rename" },                      -- Renames and updates imports if lsp.settings.renameFilesWithClasses == "always"
    },
  }, { prefix = "<leader>" })
end


vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
  pattern = { "*.spec", "*.dart" },
  callback = loadFlutterKeys,
})

return {
  'akinsho/flutter-tools.nvim',
  lazy = false,
  dependencies = {
    'nvim-lua/plenary.nvim',
    'stevearc/dressing.nvim',
  },
  opts = {
    dev_tools = {
      autostart = true,
      auto_open_browser = true,
    },
    dev_log = {
      enabled = true,
      notify_errors = false,
      open_cmd = "15split",
    },
  },
  config = true,
  init = function()
    require("telescope").load_extension("flutter")
  end
}
