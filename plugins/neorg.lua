return {
  "nvim-neorg/neorg",
  build = ":Neorg sync-parsers",
  run = ":Neorg sync-parsers", -- This is the important bit!
  dependencies = { { "nvim-lua/plenary.nvim" }, { "nvim-neorg/neorg-telescope" } },
  lazy = false,
  opts = {
    load = {
      ["core.defaults"] = {},  -- Loads default behaviour
      ["core.concealer"] = {}, -- Adds pretty icons to your documents
      ["core.export"] = {},
      ["core.completion"] = {
        config = {
          engine = "nvim-cmp",
        }
      },
      ["core.dirman"] = { -- Manages Neorg workspaces
        config = {
          workspaces = {
            notes = "~/Documents/Notas/",
            ["pdvsa-costo"] = "~/Documents/Projects/Notas/pdvsa-costo",
            tasks = "~/Documents/Tareas/",
          },
        },
      },
      ["core.integrations.telescope"] = {},
    },
  }
}
