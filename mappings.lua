return {
  n = {
    ["<Tab>"] = { "<cmd>bnext<cr>" },
    ["<S-Tab>"] = { "<cmd>bprevious<cr>" },
    ["<C-t>"] = { "<cmd>exe v:count1 . \"ToggleTerm\"<cr>" },
  },
  t = {
    ["<C-t>"] = { "<c-\\><c-n>" },
    -- ["<C-s-t>"] = { "<c-\\><c-o><cmd>ToggleTermToggleAll<cr>" },

  },
  i = {
    ["<Esc>"] = "<Nop>"
  }
}
