-- vim autocmd on file ~/.config/waybar/style.css is modified killall waybar && waybar
vim.cmd [[autocmd BufWritePost $HOME/.config/waybar/* silent !killall waybar ; $HOME/.config/waybar/bars.sh &]]
-- vim.api.nvim_create_autocmd("BufWritePost", {
--   pattern = "~/.config/waybar/style.css",
--   command = "silent !killall waybar && waybar &",
-- })


return {
  colorscheme = "rose-pine",
  options = {
    opt = {
      swapfile = false
    }
  },
  heirline = {
    colorscheme = "rose-pine",
  },
  lsp = {
    servers = {
      "dartls",
    },
    setup_handlers = {
      dartls = function(_, opts) require("flutter-tools").setup { lsp = opts } end,
      -- add custom handler
      jdtls = function(_, opts)
        vim.api.nvim_create_autocmd("Filetype", {
          pattern = "java", -- autocmd to start jdtls
          callback = function()
            if opts.root_dir and opts.root_dir ~= "" then require("jdtls").start_or_attach(opts) end
          end,
        })
      end,
    },
    config = {
      sqls = {
        on_attach = function(client, bufnr)
          require("sqls").on_attach(client, bufnr)
        end,
      },
      dartls = {
        color = {
          enabled = true,
        },
        settings = {
          showTodos = true,
          completeFunctionCalls = true,
        },
      },

      phpactor = function(opts)

        opts.init_options = {
          ["language_server.diagnostics_on_update"] = false,
          ["language_server.diagnostics_on_open"] = false,
          ["language_server.diagnostics_on_save"] = false,
        }
        return opts
      end,
      jdtls = function()
        local root_markers = { "mvnw", "gradlew", "pom.xml", "build.gradle" }
        local root_dir = require("jdtls.setup").find_root(root_markers)

        local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
        local workspace_dir = vim.fn.stdpath "data" .. "/site/java/workspace-root/" .. project_name
        if vim.fn.isdirectory(workspace_dir) ~= 1 then
          os.execute("mkdir " .. workspace_dir)
        end

        local install_path = require("mason-registry").get_package("jdtls"):get_install_path()

        local os
        if vim.fn.has "macunix" then
          os = "mac"
        elseif vim.fn.has "win32" then
          os = "win"
        else
          os = "linux"
        end

        -- return the server config
        return {
          cmd = {
            "/usr/lib/jvm/java-17-openjdk/bin/java",
            "-Declipse.application=org.eclipse.jdt.ls.core.id1",
            "-Dosgi.bundles.defaultStartLevel=4",
            "-Declipse.product=org.eclipse.jdt.ls.core.product",
            "-Dlog.protocol=true",
            "-Dlog.level=ALL",
            "-javaagent:" .. install_path .. "/lombok.jar",
            "-Xms1g",
            "--add-modules=ALL-SYSTEM",
            "--add-opens",
            "java.base/java.util=ALL-UNNAMED",
            "--add-opens",
            "java.base/java.lang=ALL-UNNAMED",
            "-jar",
            vim.fn.glob(install_path .. "/plugins/org.eclipse.equinox.launcher_*.jar"),
            "-configuration",
            install_path .. "/config_" .. os,
            "-data",
            workspace_dir,
          },
          root_dir = root_dir,
        }
      end,
    },
  },
}
